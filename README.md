# Tradukisto

Translation software for gambas projects.

## About this software
This program was born out of the need to quickly translate programs into several languages at the same time in an efficient way and using as little as possible the web translation services.
To achieve this, it takes advantage of the translations that are already available and that are hosted in shrimp projects on the hard disk.

## How-to
Tradukisto collects the data of the pre-existing translations and the new ones that are being added. To do this, two directories must be established, one from which to read projects (see below step 2) and the other from which to translate projects (see below step 3), although both can be the same.
Then the next step is to configure the output languages (see below step 4), then you have to update the data (see below step 6).
Now you are ready to translate your gambas projects.

![Form](./png/screenshoot_2.png)

 1. Configuration
 2. Adding and removing project directories with translations for reuse
 3. Adding and removing project directories to be translated
 4. Selection of output languages
 5. Overwrite data if data is not empty
 6. Refresh all
 7. Save data in translation files
 8. Translate missing texts
 9. Exit program
 10. Run the selected project program
 11. Open the directory of the current project

Note: It is also possible to use translate-shell but it is not necessary to install it.

To start translating, a project must first be selected, (see below step 1).
In the main form, the first column of the table lists all the phrases of the programme, which of course matches the language you defined in your project. Then, in the following columns, they will be filled in with the translations.
To translate, just right-click and select the first option in the menu (see below step 2) and a new tab will open in the default web browser, where the phrases will be translated.
When everything is translated, copy the translation with the web button and go back to the program by right clicking again and this time select the second menu option (see below step 3), which is to paste the data. Done, then repeat the process for as many columns to be translated as are configured.
Finally you mus save your work (see below step 4)

![Form](./png/screenshoot_1.png)


## Features
 - Reusing previous translations from gambas3 projects
 - Saves data to hard disk in a text file in JSON format.
 - Does not use databases
 - Allows great flexibility in choosing output languages
 - Edits .po files if they do not exist and creates them from the .pot file

### TODO

- Improve speed
- Add statistics and charts
- Export data to CSV
- Do the CLI version

## Supported languages

- BG, Bulgarian (български език)
- CA, Catalan (Catalá)
- CS, Czech (Čeština)
- DA, Danish (Dansk)
- DE, German (Deutsch)
- EL, Greek (ελληνικά)
- EN, English (English)
- EO, Esperanto (Esperanto)
- ES, Spanish (Castellano)
- ET, Estonian (Eesti)
- FI, Finnish (Suomi)
- FR, French (Français)
- GL, Galician (Galego)
- HU, Hungarian (Magyar)
- IT, Italian (Italiano)
- JA, Japanese (Japanese)
- LT, Lithuanian (lietuvių kalba)
- LV, Latvian (latviešu valoda)
- NL, Dutch (Nederlands)
- NO, Norwegian (Norsk)
- PL, Polish (Polski)
- PT, Portuguese (Português)
- RO, Romanian (Limba română)
- RU, Russian (Русский)
- SK, Slovak (slovenčina)
- SL, Slovenian (slovenščina)
- SV, Swedish (Svenska)
- ZH, Chinese (Chinese)

## References
This program has been developed with Gambas IDE.
http://gambas.sourceforge.net/en/main.html

## Change log

* v.0.2.34 - Fix language selection buttons vissibility on gnome3 desktop, fix error on phrases including slash "/".
* v.0.2.33 - Rewriting from scratch. Initial Commit to gitlab
